package com.hackyeah.recapka.repository;

import com.hackyeah.recapka.model.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaterialRepository extends JpaRepository<Material, Long> { }
