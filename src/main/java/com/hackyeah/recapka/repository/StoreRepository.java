package com.hackyeah.recapka.repository;

import com.hackyeah.recapka.model.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store, Long> { }
