package com.hackyeah.recapka.repository;

import com.hackyeah.recapka.model.CollectionPoint;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CollectionPointRepository extends JpaRepository<CollectionPoint, Long> { }
