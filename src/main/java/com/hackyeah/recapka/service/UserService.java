package com.hackyeah.recapka.service;

import com.hackyeah.recapka.model.User;
import com.hackyeah.recapka.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public ResponseEntity<String> addNewUser(User user) {
        Optional<User> optionalUser = userRepository.findByUsername(user.getUsername());
        if (optionalUser.isPresent()) {
            return new ResponseEntity<>("User already exists", HttpStatus.FORBIDDEN);
        } else {
            if(user.getUsername().trim().length() <= 0 || user.getPassword().trim().length() <= 0) {
                return new ResponseEntity<>("Username or password is missing", HttpStatus.NOT_ACCEPTABLE);
            }
            User newUser = new User();
            newUser.setUsername(user.getUsername());
            newUser.setFirstName(user.getFirstName());
            newUser.setLastName(user.getLastName());
            newUser.setEmail(user.getEmail());
            newUser.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userRepository.save(newUser);
            return new ResponseEntity<>("User Created", HttpStatus.OK);
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(username);
        }
        return new org.springframework.security.core.userdetails.User(user.get().getUsername(),
                user.get().getPassword(), emptyList());
    }

    public ResponseEntity<User> getUser(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
        }
    }

    public ResponseEntity<User> getUser(long userID) {
        Optional<User> optionalUser = userRepository.findById(userID);
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
        }
    }

    // Added in order to be able to reuse code for fetching users
    public User getUserEntity(String username) {
        Optional<User> userOptional = userRepository.findByUsername(username);
        return userOptional.orElse(null);
    }

    public ResponseEntity<Iterable<User>> getUsers() {
        Iterable<User> users = userRepository.findAll();
        if (users == null)
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(users, HttpStatus.OK);
    }

    public ResponseEntity<String> updateUsername(long userID, String username){
        Optional<User> optionalUser = userRepository.findById(userID);
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            User user = optionalUser.get();
            user.setUsername(username);
            userRepository.save(user);
            return new ResponseEntity<>("Done", HttpStatus.OK);
        }
    }

    public ResponseEntity<String> deleteUser(long userID){
        Optional<User> optionalUser = userRepository.findById(userID);
        if (!optionalUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            userRepository.delete(optionalUser.get());
            return new ResponseEntity<>("Done", HttpStatus.OK);
        }
    }
}
