package com.hackyeah.recapka.service;

import com.hackyeah.recapka.command.GeoFilter;
import com.hackyeah.recapka.command.StoreCommand;
import com.hackyeah.recapka.model.Store;
import com.hackyeah.recapka.model.User;
import com.hackyeah.recapka.repository.StoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class StoreService {

    private StoreRepository storeRepository;
    private UserService userService;

    public StoreService(StoreRepository storeRepository, UserService userService) {
        this.storeRepository = storeRepository;
        this.userService = userService;
    }

    public List<Store> getStores(GeoFilter geoFilter) {
        Stream<Store> storeStream = storeRepository.findAll().stream();
        if(geoFilter != null) storeStream = storeStream.filter(point -> geoFilter.checkLocation(point.getLocation()));
        return storeStream.collect(Collectors.toList());
    }

    public boolean addStore(StoreCommand command) {
        User owner = userService.getUserEntity(command.getOwnerName());
        if(owner == null) return false;

        Store store = new Store(command.getName(), command.getLocation(), owner);
        storeRepository.save(store);
        return true;
    }

    public boolean updateStore(long storeId, StoreCommand command) {
        Optional<Store> storeOptional = storeRepository.findById(storeId);
        if(!storeOptional.isPresent()) return false;

        Store store = storeOptional.get();
        if(command.getName() != null) store.setName(command.getName());
        if(command.getLocation() != null) store.setLocation(command.getLocation());
        if(command.getOwnerName() != null) {
            User owner = userService.getUserEntity(command.getOwnerName());
            if(owner == null) return false;
            store.setOwner(owner);
        }
        storeRepository.save(store);
        return true;
    }

    public boolean deleteStore(long storeId) {
        Optional<Store> storeOptional = storeRepository.findById(storeId);
        if(!storeOptional.isPresent()) return false;
        storeRepository.deleteById(storeId);
        return true;
    }
}
