package com.hackyeah.recapka.service;

import com.hackyeah.recapka.model.Material;
import com.hackyeah.recapka.model.User;
import com.hackyeah.recapka.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class EcopointService {

    private UserRepository userRepository;
    private MaterialService materialService;

    public EcopointService(UserRepository userRepository, MaterialService materialService) {
        this.userRepository = userRepository;
        this.materialService = materialService;
    }

    public Double getUserEcopoints(String userName) {
        Optional<User> userOptional = userRepository.findByUsername(userName);
        if(!userOptional.isPresent()) return null;
        return userOptional.get().getEcopoints();
    }

    public boolean submitDelivery(String userName, Map<Integer, Double> deliveryMap) {
        double sum = 0;
        for(Map.Entry<Integer, Double> entry : deliveryMap.entrySet()) {
            Material material = materialService.getMaterial(entry.getKey());
            sum += material.getExchangeRate() * entry.getValue();
        }
        return depositUserEcopoints(userName, sum);
    }

    public boolean withdrawUserEcopoints(String userName, double ecopoints) {
        return depositUserEcopoints(userName, -ecopoints);
    }

    private boolean depositUserEcopoints(String userName, double ecopoints) {
        Optional<User> userOptional = userRepository.findByUsername(userName);
        if(!userOptional.isPresent()) return false;

        User user = userOptional.get();
        user.setEcopoints(user.getEcopoints() + ecopoints);
        userRepository.save(user);
        return true;
    }
}
