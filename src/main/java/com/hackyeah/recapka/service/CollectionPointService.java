package com.hackyeah.recapka.service;

import com.hackyeah.recapka.command.CollectionPointCommand;
import com.hackyeah.recapka.command.GeoFilter;
import com.hackyeah.recapka.model.CollectionPoint;
import com.hackyeah.recapka.model.Material;
import com.hackyeah.recapka.model.User;
import com.hackyeah.recapka.repository.CollectionPointRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CollectionPointService {

    private CollectionPointRepository collectionPointRepository;
    private UserService userService;
    private MaterialService materialService;

    public CollectionPointService(CollectionPointRepository collectionPointRepository,
                                  UserService userService,
                                  MaterialService materialService) {
        this.collectionPointRepository = collectionPointRepository;
        this.materialService = materialService;
        this.userService = userService;
    }

    public List<CollectionPoint> getCollectionPoints(GeoFilter geoFilter, List<Long> materialIdsFilter) {
        Stream<CollectionPoint> pointStream = collectionPointRepository.findAll().stream();
        if(geoFilter != null) pointStream = pointStream.filter(point -> geoFilter.checkLocation(point.getLocation()));
        if(materialIdsFilter != null) pointStream = pointStream.filter(point -> {
            List<Long> pointMaterialIds = point.getMaterials().stream().map(Material::getId).collect(Collectors.toList());
            return pointMaterialIds.containsAll(materialIdsFilter);
        });
        return pointStream.collect(Collectors.toList());
    }

    public boolean addCollectionPoint(CollectionPointCommand command) {
        User owner = userService.getUserEntity(command.getOwnerName());
        if(owner == null) return false;

        List<Material> materials = materialService.getMaterials(command.getMaterialIds());
        if(materials == null) return false;

        CollectionPoint collectionPoint = new CollectionPoint(command.getName(), command.getLocation(), materials, owner);
        collectionPointRepository.save(collectionPoint);
        return true;
    }

    public boolean updateCollectionPoint(long collectionPointId, CollectionPointCommand command) {
        Optional<CollectionPoint> collectionPointOptional = collectionPointRepository.findById(collectionPointId);
        if(!collectionPointOptional.isPresent()) return false;

        CollectionPoint collectionPoint = collectionPointOptional.get();
        if(command.getName() != null) collectionPoint.setName(command.getName());
        if(command.getLocation() != null) collectionPoint.setLocation(command.getLocation());
        if(command.getMaterialIds() != null) {
            List<Material> materials = materialService.getMaterials(command.getMaterialIds());
            if(materials == null) return false;
            collectionPoint.setMaterials(materials);
        }
        if(command.getOwnerName() != null) {
            User owner = userService.getUserEntity(command.getOwnerName());
            if(owner == null) return false;
            collectionPoint.setOwner(owner);
        }
        collectionPointRepository.save(collectionPoint);
        return true;
    }

    public boolean deleteCollectionPoint(long collectionPointId) {
        Optional<CollectionPoint> collectionPointOptional = collectionPointRepository.findById(collectionPointId);
        if(!collectionPointOptional.isPresent()) return false;
        collectionPointRepository.deleteById(collectionPointId);
        return true;
    }
}
