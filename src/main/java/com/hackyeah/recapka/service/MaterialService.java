package com.hackyeah.recapka.service;

import com.hackyeah.recapka.command.MaterialCommand;
import com.hackyeah.recapka.model.Material;
import com.hackyeah.recapka.repository.MaterialRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MaterialService {

    private MaterialRepository materialRepository;

    public MaterialService(MaterialRepository materialRepository) {
        this.materialRepository = materialRepository;
    }

    public List<Material> getMaterials() {
        return materialRepository.findAll();
    }

    public Material getMaterial(long materialId) {
        return materialRepository.findById(materialId).orElse(null);
    }

    public List<Material> getMaterials(List<Long> materialIds) {
        List<Material> materials = materialIds.stream().map(this::getMaterial).collect(Collectors.toList());
        if(materials.stream().anyMatch(Objects::isNull)) return null;
        return materials;
    }

    public void addMaterial(MaterialCommand command) {
        Material material = new Material(command.getName(), command.getDescription(), command.getExchangeRate());
        materialRepository.save(material);
    }

    public boolean updateMaterial(long materialId, MaterialCommand command) {
        Optional<Material> materialOptional = materialRepository.findById(materialId);
        if(!materialOptional.isPresent()) return false;

        Material material = materialOptional.get();
        if(command.getName() != null) material.setName(command.getName());
        if(command.getDescription() != null) material.setDescription(command.getDescription());
        if(command.getExchangeRate() != null) material.setExchangeRate(command.getExchangeRate());
        materialRepository.save(material);
        return true;
    }

    public boolean deleteMaterial(long materialId) {
        Optional<Material> materialOptional = materialRepository.findById(materialId);
        if(!materialOptional.isPresent()) return false;
        materialRepository.deleteById(materialId);
        return true;
    }
}
