package com.hackyeah.recapka.security;

public class SecurityConstants {
    public static final String SECRET = "Zpyr?r^^wcRY,,YZpyr?r^^wcRY,,Y";
    public static final long EXPIRATION_TIME = 86_400_000; // 1day
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/user/register";
}
