package com.hackyeah.recapka.model;

import javax.persistence.*;

@Entity
public class Store {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    @Embedded
    private Location location;
    @ManyToOne
    private User owner;

    public Store() { }

    public Store(String name, Location location, User owner) {
        this.name = name;
        this.location = location;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
