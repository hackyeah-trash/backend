package com.hackyeah.recapka.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class CollectionPoint {

    @Id
    @GeneratedValue
    private long id;
    private String name;
    @Embedded
    private Location location;
    @ManyToMany
    private List<Material> materials;
    @ManyToOne
    private User owner;

    public CollectionPoint() { }

    public CollectionPoint(String name, Location location, List<Material> materials, User owner) {
        this.name = name;
        this.location = location;
        this.materials = materials;
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
