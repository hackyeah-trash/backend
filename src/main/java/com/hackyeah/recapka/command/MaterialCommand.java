package com.hackyeah.recapka.command;

public class MaterialCommand {

    private String name;
    private String description;
    private Double exchangeRate; // Points per kg

    public MaterialCommand() { }

    public MaterialCommand(String name, String description, Double exchangeRate) {
        this.name = name;
        this.description = description;
        this.exchangeRate = exchangeRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }
}
