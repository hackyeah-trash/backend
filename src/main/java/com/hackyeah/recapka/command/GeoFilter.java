package com.hackyeah.recapka.command;

import com.hackyeah.recapka.model.Location;

public class GeoFilter {

    private Location location;
    private double radius;

    public GeoFilter(double latitude, double longitude, double radius) {
        this.location = new Location(latitude, longitude);
        this.radius = radius;
    }

    public static GeoFilter create(Double latitude, Double longitude, Double radius) {
        if(latitude != null && longitude != null && radius != null) return new GeoFilter(latitude, longitude, radius);
        else if(latitude == null && longitude == null && radius == null) return null;
        else throw new IllegalArgumentException("Cannot create GeoFilter");
    }

    public boolean checkLocation(Location location) {
        return distanceTo(location) <= radius;
    }

    private double distanceTo(Location other) {
        double lat1Rad = Math.toRadians(location.getLatitude());
        double lat2Rad = Math.toRadians(other.getLatitude());

        double theta = location.getLongitude() - other.getLongitude();
        double dist = Math.sin(lat1Rad) * Math.sin(lat2Rad) + Math.cos(lat1Rad) * Math.cos(lat2Rad) * Math.cos(Math.toRadians(theta));
        dist = Math.acos(dist);
        dist = Math.toDegrees(dist);
        dist = dist * 60 * 1.853159616;
        return dist;
    }
}
