package com.hackyeah.recapka.command;

import com.hackyeah.recapka.model.Location;

import java.util.List;

public class CollectionPointCommand {

    private String name;
    private Location location;
    private List<Long> materialIds;
    private String ownerName;

    public CollectionPointCommand() { }

    public CollectionPointCommand(String name, Location location, List<Long> materialIds, String ownerName) {
        this.name = name;
        this.location = location;
        this.materialIds = materialIds;
        this.ownerName = ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<Long> getMaterialIds() {
        return materialIds;
    }

    public void setMaterialIds(List<Long> materialIds) {
        this.materialIds = materialIds;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}
