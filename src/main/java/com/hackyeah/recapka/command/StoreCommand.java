package com.hackyeah.recapka.command;

import com.hackyeah.recapka.model.Location;

public class StoreCommand {

    private String name;
    private Location location;
    private String ownerName;

    public StoreCommand() { }

    public StoreCommand(String name, Location location, String ownerName) {
        this.name = name;
        this.location = location;
        this.ownerName = ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}
