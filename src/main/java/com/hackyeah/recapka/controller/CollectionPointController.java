package com.hackyeah.recapka.controller;

import com.hackyeah.recapka.command.CollectionPointCommand;
import com.hackyeah.recapka.command.GeoFilter;
import com.hackyeah.recapka.model.CollectionPoint;
import com.hackyeah.recapka.service.CollectionPointService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/collectionpoints")
public class CollectionPointController {

    private CollectionPointService collectionPointService;

    public CollectionPointController(CollectionPointService collectionPointService) {
        this.collectionPointService = collectionPointService;
    }

    @GetMapping
    public List<CollectionPoint> getCollectionPoints(@RequestParam(required = false) Double lat,
                                                     @RequestParam(required = false) Double lng,
                                                     @RequestParam(required = false) Double radius,
                                                     @RequestParam(required = false) List<Long> materialIds) {
        GeoFilter geoFilter = GeoFilter.create(lat, lng, radius);
        return collectionPointService.getCollectionPoints(geoFilter, materialIds);
    }

    @PostMapping
    public void addCollectionPoint(@RequestBody CollectionPointCommand command) {
        if(!collectionPointService.addCollectionPoint(command)) throw new StatusException.NotFound();
    }

    @PutMapping(path = "/{collectionPointId}")
    public void updateCollectionPoint(@PathVariable long collectionPointId,
                                      @RequestBody CollectionPointCommand command) {
        if(!collectionPointService.updateCollectionPoint(collectionPointId, command)) throw new StatusException.NotFound();
    }

    @DeleteMapping(path = "/{collectionPointId}")
    public void deleteCollectionPoint(@PathVariable long collectionPointId) {
        if(!collectionPointService.deleteCollectionPoint(collectionPointId)) throw new StatusException.NotFound();
    }
}
