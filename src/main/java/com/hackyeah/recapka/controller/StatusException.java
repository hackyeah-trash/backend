package com.hackyeah.recapka.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface StatusException {

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    class BadRequest extends RuntimeException { }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    class NotFound extends RuntimeException { }
}
