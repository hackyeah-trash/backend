package com.hackyeah.recapka.controller;

import com.hackyeah.recapka.model.User;
import com.hackyeah.recapka.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(path = "/api/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(path = "/register")
    public ResponseEntity<String> addNewUser(@RequestBody User user) {
        return userService.addNewUser(user);
    }

    @GetMapping(path = "/{username}")
    public ResponseEntity<User> getUser(@PathVariable String username){
        return userService.getUser(username);
    }

    @GetMapping(path = "/all")
    @ApiOperation(value = "View a list of available users", response = User.class)
    public ResponseEntity<Iterable<User>> getUsers(){
        return userService.getUsers();
    }

    @PutMapping(path = "/{userID}/{username}")
    public ResponseEntity<String> updateUsername(@PathVariable long userID, @PathVariable String username) {
        return userService.updateUsername(userID, username);
    }

    @DeleteMapping(path = "/{userID}")
    public ResponseEntity<String> deleteUser(@PathVariable long userID){
        return userService.deleteUser(userID);
    }
}
