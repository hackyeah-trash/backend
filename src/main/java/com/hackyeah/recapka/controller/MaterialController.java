package com.hackyeah.recapka.controller;

import com.hackyeah.recapka.command.MaterialCommand;
import com.hackyeah.recapka.model.Material;
import com.hackyeah.recapka.service.MaterialService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/materials")
public class MaterialController {

    private MaterialService materialService;

    public MaterialController(MaterialService materialService) {
        this.materialService = materialService;
    }

    @GetMapping
    public List<Material> getMaterials() {
        return materialService.getMaterials();
    }

    @PostMapping
    public void addMaterials(@RequestBody MaterialCommand command) {
        materialService.addMaterial(command);
    }

    @PutMapping(path = "/{materialId}")
    public void updateMaterial(@PathVariable long materialId,
                                      @RequestBody MaterialCommand command) {
        boolean result = materialService.updateMaterial(materialId, command);
        if(!result) throw new StatusException.NotFound();
    }

    @DeleteMapping(path = "/{materialId}")
    public void deleteMaterial(@PathVariable long materialId) {
        boolean result = materialService.deleteMaterial(materialId);
        if(!result) throw new StatusException.NotFound();
    }
}
