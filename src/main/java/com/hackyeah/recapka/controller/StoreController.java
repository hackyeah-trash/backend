package com.hackyeah.recapka.controller;

import com.hackyeah.recapka.command.GeoFilter;
import com.hackyeah.recapka.command.StoreCommand;
import com.hackyeah.recapka.model.Store;
import com.hackyeah.recapka.service.StoreService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api/stores")
public class StoreController {

    private StoreService storeService;

    public StoreController(StoreService storeService) {
        this.storeService = storeService;
    }

    @GetMapping
    public List<Store> getStores(@RequestParam(required = false) Double lat,
                                 @RequestParam(required = false) Double lng,
                                 @RequestParam(required = false) Double radius) {
        GeoFilter geoFilter = GeoFilter.create(lat, lng, radius);
        return storeService.getStores(geoFilter);
    }

    @PostMapping
    public void addStore(@RequestBody StoreCommand command) {
        if(!storeService.addStore(command)) throw new StatusException.NotFound();
    }

    @PutMapping(path = "/{storeId}")
    public void updateStore(@PathVariable long storeId,
                                      @RequestBody StoreCommand command) {
        if(!storeService.updateStore(storeId, command)) throw new StatusException.NotFound();
    }

    @DeleteMapping(path = "/{storeId}")
    public void deleteMaterial(@PathVariable long storeId) {
        if(!storeService.deleteStore(storeId)) throw new StatusException.NotFound();
    }
}
