package com.hackyeah.recapka.controller;

import com.hackyeah.recapka.service.EcopointService;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(path = "/api/user")
public class EcopointController {

    private EcopointService ecopointService;

    public EcopointController(EcopointService ecopointService) {
        this.ecopointService = ecopointService;
    }

    @GetMapping(path = "/{userName}/ecopoints")
    public double getUserEcopoints(@PathVariable String userName) {
        return ecopointService.getUserEcopoints(userName);
    }

    @PostMapping(path = "/{userName}/ecopoints")
    public void submitDelivery(@PathVariable String userName,
                               @RequestBody Map<Integer, Double> deliveryMap) {
        boolean result = ecopointService.submitDelivery(userName, deliveryMap);
        if(!result) throw new StatusException.NotFound();
    }

    @DeleteMapping(path = "/{userName}/ecopoints")
    public void withdrawUserEcopoints(@PathVariable String userName,
                                      @RequestBody int ecopoints) {
        boolean result = ecopointService.withdrawUserEcopoints(userName, ecopoints);
        if(!result) throw new StatusException.NotFound();
    }
}
